/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package httputil;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Peter
 */
public class HttpUtilTest {
    
    public HttpUtilTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of getHtml method, of class HttpUtil.
     */
    @Test
    public void testGetHtml() throws Exception {
        System.out.println("getHtml");
        HttpUtil instance = new HttpUtil();
        String expResult = "<p>This is the default web page for this server.</p>" +
"<p>The web server software is running but no content has been added, yet.</p>" +
"</body></html>";
        String result = instance.getHtml();
        assertEquals(expResult, result);
    }

    /**
     * Test of get42 method, of class HttpUtil.
     */
    @Test
    public void testGet42() {
        System.out.println("get42");
        HttpUtil instance = new HttpUtil();
        int expResult = 42;
        int result = instance.get42();
        assertEquals(expResult, result);
    }
    
}
