/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package httputil;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
/**
 *
 * @author Peter
 */
public class HttpUtil {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws MalformedURLException {
        // TODO code application logic here
        HttpUtil util = new HttpUtil();
        
        System.out.println(util.getHtml());
        System.out.println(util.get42());
    }
    
    private URL url;

    public void HttpHelper() {
        try {
            url = new URL("http://www.google.com");
        } catch (MalformedURLException e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public String getHtml() throws MalformedURLException {
        url = new URL("http://peter-johennecken.de");
        HttpURLConnection con;
        String ret = "";
        try {
            con = (HttpURLConnection) url.openConnection();
            //con.setRequestMethod("GET");
            con.setUseCaches(false);
            System.out.println("Response: " + con.getResponseCode() + "\n" + con.getResponseMessage());
            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) ret = "false";
            else {
                BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                System.out.println(reader.readLine());
                while ((inputLine = reader.readLine()) != null) {
                    ret += inputLine;
                }
                reader.close();
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return ret;
    }

    public int get42() {
        return 42;
    }
    
}
